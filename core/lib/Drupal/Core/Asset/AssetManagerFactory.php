<?php

/**
 * @file
 * Contains Drupal\Core\Asset\AssetFactory.
 */

namespace Drupal\Core\Asset;

/**
 * Defines the asset factory.
 */
class AssetManagerFactory {

  /**
   * Instantiates an asset class for a given asset type.
   *
   * Classes implementing Drupal\Core\Assets\AssetInterface can register
   * themselves for specific types.
   *
   * @param string $type
   *   The type for which an asset object should be returned.
   *
   * @return Drupal\Core\Assets\AssetInterface
   *   The asset object associated with the specified type.
   */
  public static function get($type) {
    // Check whether there is a custom class defined for the requested type or
    // use the default 'type' definition otherwise.
    $asset_types = self::getTypes();
    $class = $asset_types[$type];
    return new $class($type);
  }

  /**
   * Returns a list of asset types for this site.
   *
   * @return array
   *   An associative array with types as keys, and asset class names as
   *   value.
   */
  public static function getTypes() {
    // @todo Just copying CacheFactory here, but really should use config.
    // See _aggregator_get_variables().
    // Perhaps store in system module config.
    // Need to return other config, too, bundler and packager variables for each type. 
    global $conf;
    $asset_types = isset($conf['asset_classes']) ? $conf['asset_classes'] : array();
    // Ensure there is a default 'generic' definition.
    $asset_types += array('css' => 'Drupal\Core\Asset\CssAssetManager');
    // $asset_types += array('js' => 'Drupal\Core\Asset\JsAssetManager');
    return $asset_types;
  }

}

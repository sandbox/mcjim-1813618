<?php

/**
 * @file
 * Definition of Drupal\Core\Assets\AssetInterface.
 */

namespace Drupal\Core\Asset;

/**
 * Defines an interface for asset collections.
 *
 * @see assets()
 */
abstract class AssetManager {

  /**
   * Returns array of assets.
   *
   * @param $group
   *   The group of assets to return.
   *
   * @return
   *   Array of assets as returned by drupal_add_css() or drupal_add_js().
   */
  abstract function get($group = NULL);

  /**
   * Renders the assets into the appropriate HTML.
   *
   * @return
   *   HTML string.
   */
  abstract function render();

  /**
   * Defines the current assets.
   *
   * @param array $data
   *   Array of assets as returned by drupal_add_css() or drupal_add_js()..
   */
  function set($data = NULL) {

  }
  /**
   * Process the assets, applying filters provided by plugins.
   */
  function process() {

  }

  /**
   * Sorts the assets.
   */
  function sort() {

  }
}

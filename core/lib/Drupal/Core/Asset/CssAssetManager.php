<?php

/**
 * @file
 * Definition of Drupal\Core\Asset\CssAssetManager.
 */

namespace Drupal\Core\Asset;

/**
 * Defines a default Asset implementation.
 *
 * This is Drupal's default CSS Asset implementation.
 */
class CssAssetManager extends AssetManager {

  public $assets = array();

  /**
   * Implements Drupal\Core\Assets\AssetCollectionInterface::get().
   */
  function get($group = NULL) {
    $assets = array();
    if ($group === NULL) {
      $assets = $this->assets;
    }
    foreach ($this->assets as $file) {
      if (isset($file['group']) && $file['group'] == $group) {
        $assets[] = $file;
      }
    }
    return $assets;
  }

  /**
   * Implements Drupal\Core\Assets\AssetCollectionInterface::set().
   */
  function set($css = NULL) {
    if (!isset($css)) {
      $css = drupal_add_css();
    }
      $this->assets = $css;
  }

  /**
   * Implements Drupal\Core\Assets\AssetCollectionInterface::set().
   */
  function sort() {
    // Sort CSS items, so that they appear in the correct order.
    uasort($this->assets, 'drupal_sort_css_js');

    // Remove the overridden CSS files. Later CSS files override former ones.
    $previous_item = array();
    foreach ($this->assets as $key => $item) {
      if ($item['type'] == 'file') {
        // If defined, force a unique basename for this file.
        $basename = isset($item['basename']) ? $item['basename'] : drupal_basename($item['data']);
        if (isset($previous_item[$basename])) {
          // Remove the previous item that shared the same base name.
          unset($this->assets[$previous_item[$basename]]);
        }
        $previous_item[$basename] = $key;
      }
    }
  }

  /**
   * Implements Drupal\Core\Assets\AssetCollectionInterface::set().
   */
  function render() {
    // Render the HTML needed to load the CSS.
    $styles = array(
      '#type' => 'styles',
      '#items' => $this->assets,
    );
    return drupal_render($styles);
  }

}
